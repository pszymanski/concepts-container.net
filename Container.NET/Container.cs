﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace PK.Container
{
    public class Container: IContainer
    {
        protected IDictionary<Type, Type> providedInterfaces = new Dictionary<Type, Type>();
        protected IDictionary<Type, Object> instances = new Dictionary<Type, Object>();
        protected IDictionary<Type, Delegate> providers = new Dictionary<Type, Delegate>();

        public void Register(Assembly assembly)
        {
            foreach (var item in assembly.GetExportedTypes())
                Register(item);
        }

        public void Register(Type type)
        {
            foreach (var implementedInterface in type.GetInterfaces())
                if (!providedInterfaces.ContainsKey(implementedInterface))
                    providedInterfaces.Add(implementedInterface, type);
                else
                    providedInterfaces[implementedInterface] = type;
        }

        public void Register<T>(T impl) where T : class
        {
            Type type = typeof(T);
            foreach (var implementedInterface in type.GetInterfaces())
                if (!providedInterfaces.ContainsKey(implementedInterface))
                {
                    providedInterfaces.Add(implementedInterface, type);
                    instances.Add(implementedInterface, impl);
                }
                else
                {
                    providedInterfaces[implementedInterface] = type;
                    instances[type] = impl;
                }
            if(type.IsInterface)
                if (instances.ContainsKey(typeof(T)))
                    instances[typeof(T)] = impl;
                else
                    instances.Add(typeof(T), impl);
        }

        public void Register<T>(Func<T> provider) where T : class
        {
            Type type = typeof(T);
            foreach (var implementedInterface in type.GetInterfaces())
                if (!providedInterfaces.ContainsKey(implementedInterface))
                {
                    providedInterfaces.Add(implementedInterface, type);
                    providers.Add(implementedInterface, provider);
                }
                else
                {
                    providedInterfaces[implementedInterface] = type;
                    providers[type] = provider;
                }
            if(type.IsInterface)
                if (providers.ContainsKey(typeof(T)))
                    providers[typeof(T)] = provider;
                else
                    providers.Add(typeof(T), provider);

        }

        public T Resolve<T>() where T : class
        {
            return Resolve(typeof(T)) as T;
        }

        public object Resolve(Type type)
        {
            if (instances.ContainsKey(type))
                return instances[type];
            if (providers.ContainsKey(type))
                return providers[type].DynamicInvoke();
            if (providedInterfaces.ContainsKey(type))
            {
                ConstructorInfo constructor = providedInterfaces[type].GetConstructors()[0];

                int constructor_interfaces = 0;
                foreach (var parameter in constructor.GetParameters())
                    if (parameter.ParameterType.IsInterface)
                        constructor_interfaces++;

                foreach (var cnstrctr in providedInterfaces[type].GetConstructors())
                {
                    int cnstrctr_interfaces = 0;
                    
                    foreach (var parameter in cnstrctr.GetParameters())
                        if (parameter.ParameterType.IsInterface)
                            cnstrctr_interfaces++;
                    if (cnstrctr_interfaces > constructor_interfaces)
                    {
                        constructor = cnstrctr;
                        constructor_interfaces = cnstrctr_interfaces;
                    }
                }
                        
                List<object> parameters = new List<object>();
                foreach (var parameter in constructor.GetParameters())
                {
                    var prmtr = Resolve(parameter.ParameterType);
                    if (prmtr == null)
                        throw new UnresolvedDependenciesException();
                    parameters.Add(prmtr);
                }

                return constructor.Invoke(parameters.ToArray());
            }
            return null;
        }
    }
}
