﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lab4.Contract;

namespace Lab4.Component1
{
    public class Ekspres: IEkspres
    {
        protected IKawa kawa;
       
        public Ekspres(IKawa kawa)
        {
            this.kawa = kawa;
           // RegisterRequiredInterface<IKawa>();
            //RegisterProvidedInterface<IEkspres>(this);
        }


        

        public IKawa InstancjaKawy()
        {
            return kawa;
        }

      

        public int Kawa()
        {
            return kawa.Kawa();
        }

        public bool Mleko()
        {
            return kawa.Mleko();
        }       
    }
}
