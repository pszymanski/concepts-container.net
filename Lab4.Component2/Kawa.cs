﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using ComponentFramework;
using Lab4.Contract;

namespace Lab4.Component2
{
    public class Kawa : IKawa
    {
        private int ilość;
        private bool mleko;

        public Kawa()
        {
            //RegisterProvidedInterface(typeof(IKawa),this);
            this.ilość = 10;
            this.mleko = true;
        }
        int Contract.IKawa.Kawa()
        {
            return ilość;
        }

        bool Contract.IKawa.Mleko()
        {
            return mleko;
        }



    }
}
