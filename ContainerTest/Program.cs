﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ContainerTest
{
    class Program
    {
        static void Main(string[] args)
        {
            PK.Container.Container k = new PK.Container.Container();
            k.Register(typeof(Lab4.Component1.Ekspres));
            k.Register(typeof(Lab4.Component2.Kawa));
            //k.Register(typeof(Lab4.Component1.Ekspres));
            Lab4.Component1.Ekspres e = k.Resolve<Lab4.Contract.IEkspres>() as Lab4.Component1.Ekspres;
            Console.WriteLine(e.InstancjaKawy().Mleko());
            Console.Read();
        }
    }
}
